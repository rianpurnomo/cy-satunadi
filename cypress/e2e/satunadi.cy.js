import 'cypress-xpath';

describe('SATUNADI', () => {
  const Url= "https://satunadi.id"
  beforeEach(() => {
    cy.viewport('macbook-16');
    cy.visit(Url);
    cy.wait(2000);
    //Preset	width	height
    // ipad-2	768	1024
    // ipad-mini	768	1024
    // iphone-3	320	480
    // iphone-4	320	480
    // iphone-5	320	568
    // iphone-6	375	667
    // iphone-6+	414	736
    // iphone-7	375	667
    // iphone-8	375	667
    // iphone-x	375	812
    // iphone-xr	414	896
    // iphone-se2	375	667
    // macbook-11	1366	768
    // macbook-13	1280	800
    // macbook-15	1440	900
    // macbook-16	1536	960
    // samsung-note9	414	846
    // samsung-s10	360	760
  });
  describe('Dashboard', () => {
    it('Navbar', () => {
      cy.get('[data-cy="atf"]').scrollIntoView();
      cy.get('[data-cy="atf"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-1"]').should('exist');
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
    });
    it('Title', () => {
      cy.get('[data-cy="atf-main-title"]').should('contain', 'One Digital Health Solution');
      cy.get('[data-cy="atf-title"]').should('contain', 'SATUNADI by Telkom Indonesia');
      // cy.get('[data-cy="atf-desc"]').should('contain','Sistem pengelolaan rumah sakit yang dirancang khusus untuk Anda agar tenaga kesehatan dapat melayani pasien secara efektif dan efisien.');
      cy.get('[data-cy="atf-desc"]').should('contain','Terobosan terbaru dalam pengelolaan rumah sakit yang dirancang khusus agar tenaga kesehatan dapat melayani pasien secara efektif dan efisien.');
      cy.get('[data-cy="atf-try-btn"]').should('contain', 'Coba Sekarang');
      cy.get('[data-cy="atf-more-btn"]').should('contain', 'Pelajari Lebih Lanjut');
      cy.wait(2000);
    });
    it('Description', () => {
      cy.get('[id="about"]').scrollIntoView();
      cy.get('[id="about"]').should('exist');
      cy.get('[data-cy="atf-more-btn"]').click();
      cy.get(2000);  
      // cy.get('button')
      // .contains('Play')
      // .click()
      // .then((button) => {
        // const text = button.text();
        // Melakukan sesuatu dengan teks 'Play'
        // console.log(text);
      // });
      //description
      cy.get('[data-cy="about-title"]').should('contain','Tentang Kami');
      cy.get('[data-cy="about-desc"]').should('exist');
      // cy.get('[data-cy="about-desc-paragraph-1"]').should('contain','Sebuah sistem informasi manajemen rumah sakit yang dirancang khusus menggunakan konsep (ERP Enterprise Resource Planning) untuk mengelola seluruh aspek operasional rumah sakit yang mengintegrasikan seluruh unit/bagian/instalasi yang ada di Rumah Sakit baik layanan medis maupun nonmedis.');
      cy.get('[data-cy="about-desc-paragraph-2"]').should('contain','Aplikasi ini berbasis Cloud SaaS based yang memiliki kemampuan dalam memproses dan mengintegrasikan seluruh alur proses bisnis layanan kesehatan dalam bentuk jaringan koordinasi, pelaporan dan prosedur administrasi terintegrasi untuk memperoleh informasi secara cepat, tepat dan akurat.');
    });
    it('play video', () => {
      //play video
      // cy.get('[data-cy="video-play-btn"]').click();
      cy.get('[data-cy="video-play-btn"]').as('videoElement');
      cy.get('@videoElement').should('be.visible').then(() => {
      // Mengakses properti 'Player' setelah elemen video selesai dimuat
      // cy.get('@videoElement').invoke('Player')
      cy.get('[data-cy="atf-more-btn"]').scrollIntoView();
      cy.wait(3000);
      cy.get('@videoElement').then(($video) => {
        const player = $video.prop('Player');
        // Lakukan sesuatu dengan nilai properti 'Player' yang diperoleh
        const play = $video[0].click();
        cy.get('[data-cy="atf-more-btn"]').scrollIntoView();
        cy.wait(4000);
      });
      });

      // cy.get('[data-cy="atf-more-btn"]').scrollIntoView();
      // cy.wait(10000); // 1 m
      // cy.get('[data-cy="video-play-btn"]').pause();

    });
    it('Service', () => {
      // cy.get('.my-element').scrollIntoView();
      //head
      cy.get('[data-cy="services"]').scrollIntoView();
      cy.get('[data-cy="services-title"]').should('contain', 'Keunggulan Kami bagi Fasilitas Kesehatan');
      cy.get('[data-cy="services-desc"]').should('contain', 'Nikmati berbagai keunggulan SATUNADI untuk peningkatan layanan dan manajemen rumah sakit Anda.');
      // cy.get('[class="mt-12 grid md:grid-cols-2 lg:grid-cols-3 gap-x-[2.375rem] gap-y-12 lg:justify-between"]').should('exist');
      cy.wait(2000);
      //service-1
      cy.get('[alt="icon-menkes"]').should('exist')
      cy.get('[data-cy="services-1-title"]').should('contain', 'Terintegrasi SatuSehat')
      cy.get('[data-cy="services-1-desc"]').should('contain', 'Rekam medis elektronik SATUNADI  telah terintegrasi dengan SatuSehat sesuai Permenkes 24 tahun 2022 dan standar FHIR HL-7.');
      cy.get('[data-cy="services"]').scrollIntoView();
      cy.wait(2000);
      //service-2
      cy.get('[alt="icon-promotion"]').should('exist');
      cy.get('[data-cy="services-2-title"]').should('contain', 'Terintegrasi Supra System');
      cy.get('[data-cy="services-2-desc"]').should('contain', "SATUNADI telah terintegrasi dengan Supra System berupa asuransi BPJS, INACBG's, SISRUTE, SIRANAP dan SIRSONLINE dari Kemenkes.");
      cy.get('[data-cy="services"]').scrollIntoView();
      cy.wait(2000);
      //service-3
      cy.get('[alt="icon-computer"]').should('exist');
      cy.get('[data-cy="services-3-title"]').should('contain', 'Multiplatform Web dan Mobile');
      cy.get('[data-cy="services-3-desc"]').should('contain', 'SATUNADI dapat diakses melalui web dan mobile sehingga memudahkan tenaga kesehatan dan operasional rumah sakit.');
      cy.get('[data-cy="services"]').scrollIntoView();
      cy.wait(2000);
      //service-4
      cy.get('[alt="icon-data"]').should('exist');
      cy.get('[data-cy="services-4-title"]').should('contain', 'Perlindungan Data Terbaik');
      cy.get('[data-cy="services-4-desc"]').should('contain', 'Seluruh data rumah sakit dan pasien terjamin keamanannya dengan perlindungan terbaik.');
      cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-5
      cy.get('[alt="icon-monitor"]').should('exist');
      cy.get('[data-cy="services-5-title"]').should('contain', 'Real-time Monitoring');
      cy.get('[data-cy="services-5-desc"]').should('contain', 'Monitoring data secara real-time memudahkan manajemen dalam memantau dan menganalisis operasional rumah sakit.');
      cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-6
      cy.get('[alt="icon-maintain"]').should('exist');
      cy.get('[data-cy="services-6-title"]').should('contain','Bebas Pemeliharaan');
      cy.get('[data-cy="services-6-desc"]').should('contain','SATUNADI tidak membutuhkan pemeliharaan rutin sehingga tidak mengganggu operasional rumah sakit.');
      cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-7
      cy.get('[alt="icon-deal"]').should('exist');
      cy.get('[data-cy="services-7-title"]').should('contain', 'Integrasi Layanan Rumah Sakit');
      cy.get('[data-cy="services-7-desc"]').should('contain', 'Dengan terintegrasinya layanan dan operasional rumah sakit melalui SATUNADI, maka kepuasan pasien akan semakin baik.');
      cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
      //service-8
      cy.get('[alt="icon-plan"]').should('exist');
      cy.get('[data-cy="services-8-title"]').should('contain', 'Peningkatan Efisiensi Kerja');
      cy.get('[data-cy="services-8-desc"]').should('contain', 'Monitoring data real-time akan mempermudah koordinasi sehingga operasional dan layanan rumah sakit lebih efisien.');
      cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
      //service-9
      cy.get('[alt="icon-cloud"]').should('exist');
      cy.get('[data-cy="services-9-title"]').should('contain', 'Kemudahan Evaluasi Bisnis');
      cy.get('[data-cy="services-9-desc"]').should('contain', 'Manajemen rumah sakit akan lebih mudah dalam pemantauan operasional serta pengambilan keputusan bisnis.');
      cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
    });
    it('Package', () => {
      // head
      cy.get('[data-cy="packages"]').scrollIntoView();
      cy.get('[data-cy="packages-title"]').should('contain', 'Pilihan Paket Sesuai Kebutuhan Anda');
      cy.get('[data-cy="packages-desc"]').should('contain', 'Berbagai paket langganan yang dapat dipilih sesuai kebutuhan rumah sakit Anda.');
      // package-1
      cy.get('[class="rounded-3xl bg-white border border-black-200 shadow-el-2 p-6"]').should('exist');
      cy.get('[data-cy="package-1-title"]').should('exist');
      cy.get('[data-cy="package-1-title"]').should('contain', 'Paket Basic');
      cy.contains('Tersedia beberapa modul yang dapat membantu dalam mendigitalisasi faskes Anda. Berikut ialah beberapa modul yang disediakan paket ini.').should('exist');
      cy.get('[data-cy="package-1-list-first"]').should('exist');
      cy.get('[data-cy="package-1-list-second"]').should('exist');
      cy.get('[data-cy="packages-title"]').scrollIntoView();
      cy.wait(2000);
      // package-1 learn more button
      cy.get('[data-cy="learn-more-package-btn"]').should('exist');
      cy.xpath('//*[@id="__next"]/main/section[4]/div/div[2]/div[1]/div[3]/button').click();
      cy.get('[data-cy="product-page-title"]').should('contain','RINCIAN MODUL SATUNADI');
      cy.get('[data-cy="product-page-desc"]').should('contain','Berikut adalah rincian modul SATUNADI untuk berbagai operasional rumah sakit Anda')
      // cy.wait(2000);
      // package-2
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[class="rounded-3xl bg-white border border-black-200 shadow-el-2 p-6"]').should('exist');
      cy.get('[data-cy="package-2-title"]').should('exist');
      cy.get('[data-cy="package-2-title"]').should('contain', 'Paket Advanced');
      cy.contains('Sudah');
      cy.contains('termasuk seluruh modul dalam paket basic');
      cy.contains('serta beberapa modul eksklusif lainya. Berikut ialah beberapa modul yang disediakan paket ini.');
      cy.wait(2000);
      // cy.xpath('//*[@id="__next"]/main/section[4]/div/div[2]/div[2]/div[1]/p').should('contain','Berikut adalah beberapa modul yang disediakan paket ini.');
      cy.get('[data-cy="package-2-list-first"]').should('exist');
      cy.get('[data-cy="package-2-list-second"]').should('exist');
      cy.get('[data-cy="packages-title"]').scrollIntoView();
      cy.wait(2000);
      // package-2 learn more button
      cy.get('[data-cy="learn-more-package-btn"]').should('exist');
      cy.xpath('//*[@id="__next"]/main/section[4]/div/div[2]/div[1]/div[3]/button').click();
      cy.get('[data-cy="product-page-title"]').should('contain','RINCIAN MODUL SATUNADI');
      cy.get('[data-cy="product-page-desc"]').should('contain','Berikut adalah rincian modul SATUNADI untuk berbagai operasional rumah sakit Anda');
      cy.wait(2000);
    });
    it('Content', () => {
      // body & head
      cy.get('[data-cy="content-title"]').should('contain', 'Artikel & Berita');
      cy.get('[data-cy="content-desc"]').should('contain', 'Ikuti berbagai perkembangan layanan SATUNADI terbaru');
      // cy.get('[alt="article-img"]').should('exist');
      // cy.get('[data-cy="testimony"]').scrollIntoView();
      cy.get('[data-cy="packages"]').scrollIntoView();
      cy.wait(2000);
      // more contain
      cy.get('[data-cy="content-more-btn"]').should('exist');
      cy.get('[data-cy="content"]').scrollIntoView();
      cy.get('[data-cy="content-more-btn"]').click();
      cy.wait(2000);
    });
    it('Coba Demo', () => {
      cy.get('[data-cy="try-demo-title"]').should('contain','Coba Demo Aplikasi');
      cy.get('[data-cy="try-demo-desc"]').should('contain','Mau mencoba demo aplikasi? Yuk jawab beberapa pertanyaan ini!');
      cy.get('[data-cy="content-more-btn"]').scrollIntoView();
      cy.wait(5000);
    })
    it('Footer', () => {
      // Social & address
      cy.get('[data-cy="social-1"]').scrollIntoView();
      cy.get('[data-cy="footer-sponsor"]').should('exist');
      cy.get('[data-cy="leap-logo"]').should('exist');
      cy.get('[data-cy="footer-satunadi-logo"]').should('exist');
      cy.get('[data-cy="footer-address"]').should('contain','Menara Multimedia Lantai 14');
      cy.get('[data-cy="footer-socials"]').should('exist');
      //Linkedin
      cy.get('[data-cy="social-1"]').should('have.attr', 'href', 'https://www.linkedin.com/company/satunadi-id');
      // cy.get('[alt="Icon Linkedin"]').should('exist')
      cy.get('[data-cy="social-1"]').click();
      cy.wait(2000);
      //Instagram
      cy.visit(Url)
      cy.get('[data-cy="social-2"]').should('have.attr', 'href', 'https://www.instagram.com/satunadi.id');
      // cy.get('[alt="Icon Instagram"]').should('exist');
      cy.get('[data-cy="social-2"]').click();
      //Facebook
      cy.get('[data-cy="social-3"]').should('have.attr', 'href', 'https://www.facebook.com/people/Satunadi/61552280801346/');
      // cy.get('[alt="Icon Facebook"]').should('exist');
      cy.get('[data-cy="social-3"]').click();
      //Youtube
      cy.get('[data-cy="social-4"]').should('have.attr', 'href', 'https://www.youtube.com/channel/UCgiu6xUp1RNfjkwtTYzMhUA');
      // cy.get('[alt="Icon Youtube"]').should('exist');
      cy.get('[data-cy="social-4"]').click();
      //Tiktok
      cy.get('[data-cy="social-5"]').should('have.attr', 'href', 'https://www.tiktok.com/@satunadi.id');
      // cy.get('[alt="Icon Ti ktok"]').should('exist');
      cy.get('[data-cy="social-5"]').click();
      cy.wait(2000);
      // product & services - more link - copyright
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-producs-&-services"]').should('contain', 'Produk & Layanan');
      cy.get('[data-cy="footer-product-service-1"]').should('contain','Paket Basic');
      cy.get('[data-cy="footer-product-service-2"]').should('contain','Paket Advanced');
      cy.get('[data-cy="footer-product-service-3"]').should('contain','Modul Add On');
      cy.wait(2000);
      //Jelajahi lebih lanjut
      cy.get('[data-cy="footer-more-links"]').should('contain', 'Jelajah Lebih Lanjut');
      //tentang kami
      cy.get('[data-cy="menu-1"]').should('contain','Tentang Kami');
      cy.get('[data-cy="menu-1"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="menu-1"]').click()
      cy.wait(2000);
      //product
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-2"]').should('contain','Produk');
      cy.get('[data-cy="menu-2"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-2"]').click()
      cy.wait(2000);
      //artikel & berita
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-3"]').should('contain','Artikel & Berita');
      cy.get('[data-cy="menu-3"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-3"]').click()
      cy.wait(2000);
      //hubungi kami
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-4"]').should('contain','Hubungi Kami');
      cy.get('[data-cy="menu-4"]').scrollIntoView()
      cy.get(2000);
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      // cy.get('[alt="Logo Satunadi"]').click();
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-copyright"]').should('contain', '©2023 SATUNADI. All rights reserved');
      cy.wait(2000);
    });
  });
  describe('About Us', () => {
    it('Navbar', () => {
      cy.get('[data-cy="menu-1"]').click();
      cy.get('[data-cy="navbar"]').scrollIntoView();
      cy.get('[data-cy="menu-1"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.wait(1000);
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
      cy.wait(1000);
    });
    it('Going about us', () => {
      cy.get('[data-cy="menu-1"]').click();
      cy.get('[data-cy="about-page-title"]').should('contain', 'TENTANG SATUNADI');
      cy.get('[data-cy="about-page-desc"]').should('contain', 'Terobosan terbaru dalam pengelolaan rumah sakit yang dirancang khusus agar tenaga kesehatan dapat melayani pasien secara efektif dan efisien.');
      // cy.xpath('//*[@id="__next"]/main/section[2]/div/div[1]/div/div/img').should('exist');
      cy.wait(2000);
      //description
      cy.contains('Kementerian Kesehatan telah mengeluarkan Permenkes No. 24 Tahun 2022, di mana diwajibkan bagi seluruh fasilitas kesehatan untuk memiliki Rekam Medis Elektronik (RME).').should('exist');
      cy.contains('PT Telekomunikasi Indonesia (TELKOM) sebagai penyelenggara telekomunikasi terbesar di Indonesia telah membuat layanan SATUNADI, di mana platform kesehatan berbasis SaaS ini dapat memberikan solusi kepada penyedia layanan kesehatan di Indonesia dalam bentuk Sistem Informasi Manajemen Rumah Sakit (SIMRS), Telemedicine dan Health-ID yang terintegrasi dengan transfer data RME secara real-time.').should('exist');
      // cy.contains('SATUNADI adalah sebuah sistem informasi manajemen rumah sakit (SIMRS) yang dirancang khusus menggunakan konsep ERP (Enterprise Resource Planning), untuk mengelola seluruh aspek operasional rumah sakit yang mengintegrasikan seluruh unit/ bagian/ instalasi yang  ada  di  rumah  sakit  baik  layanan  medis  maupun  nonmedis.').should('exist');
      // cy.contains('Aplikasi SIMRS SATUNADI berbasis Cloud SaaS yang memiliki kemampuan dalam memproses dan mengintegrasikan seluruh alur proses bisnis   layanan   kesehatan   dalam   bentuk   jaringan   koordinasi,   pelaporan   dan   prosedur administrasi  terintegrasi untuk  memperoleh  informasi  secara  cepat,  tepat  dan  akurat.').should('exist');
    });
    it('play video', () => {
      cy.get('[data-cy="menu-1"]').click();
      //play video
      cy.get('[data-cy="video-play-btn"]').as('videoElement');
      cy.get('@videoElement').should('be.visible').then(() => {
      // Mengakses properti 'Player' setelah elemen video selesai dimuat
      cy.wait(3000);
      cy.get('@videoElement').then(($video) => {
        const player = $video.prop('Player');
        // Lakukan sesuatu dengan nilai properti 'Player' yang diperoleh
        const play = $video[0].click();
        cy.wait(4000);
      });
      });
    });
    it('Collaborate Title', () => {
      //title
      cy.get('[data-cy="menu-1"]').click(); 
      cy.get('[data-cy="navbar"]').scrollIntoView();
      cy.get('[data-cy="collaborate-title"]').should('contain','Keunggulan Kami dalam Pelayanan Pasien');
      cy.get('[data-cy="collaborate-desc"]').should('contain','SATUNADI akan mempermudah penanganan pasien oleh tenaga kesehatan pada setiap tahapan pelayanan pasien.');
      // cy.get('[alt="collaborate-image"]').should('exist');
      //p-1
      cy.xpath('//*[@id="__next"]/main/section[3]/div/div/div[2]/div[1]/div[1]/div/span').should('exist');
      cy.get('[data-cy="title-collabroate-item-1"]').should('contain','Booking Layanan');
      cy.get('[data-cy="desc-collabroate-item-1"]').should('contain','SATUNADI menyediakan platform booking dokter dan pendaftaran online agar pasien tidak perlu antre.');
      //p-2
      cy.xpath('//*[@id="__next"]/main/section[3]/div/div/div[2]/div[2]/div[1]/div/span').should('exist');
      cy.get('[data-cy="title-collabroate-item-2"]').should('contain','Check In Pasien');
      cy.get('[data-cy="desc-collabroate-item-2"]').should('contain','Dengan SATUNADI, check in pasien di loket pendaftaran akan lebih cepat dan langsung terintegrasi dengan BPJS.');
      //p-3
      cy.xpath('//*[@id="__next"]/main/section[3]/div/div/div[2]/div[3]/div[1]/div/span').should('exist');
      cy.get('[data-cy="title-collabroate-item-3"]').should('contain','Konsultasi dan Tindakan Medis');
      cy.get('[data-cy="desc-collabroate-item-3"]').should('contain','Hasil konsultasi dan tindakan medis pasien akan langsung tersimpan dalam bentuk rekam medis elektronik yang terintegrasi dengan SatuSehat Kemenkes.');
      //p-4
      cy.xpath('//*[@id="__next"]/main/section[3]/div/div/div[2]/div[4]/div[1]/div/span').should('exist');
      cy.get('[data-cy="title-collabroate-item-4"]').should('contain','Pembayaran dan Check Out');
      cy.get('[data-cy="desc-collabroate-item-4"]').should('contain','Proses pembayaran menjadi lebih efisien dengan integrasi INACBGs yang dapat menentukan cakupan tindakan yang ditanggung oleh BPJS.');
      cy.get('[data-cy="collaborate-title"]').scrollIntoView();
      cy.wait(20000);
    });
    it('Service', () => {
      // cy.get('.my-element').scrollIntoView();
      //head
      cy.get('[data-cy="menu-1"]').click(); 
      cy.get('[data-cy="collaborate-title"]').scrollIntoView();
      cy.get('[data-cy="about-benefit-title"]').should('contain', 'Keunggulan Kami bagi Fasilitas Kesehatan');
      cy.get('[data-cy="about-benefit-desc"]').should('contain', 'Nikmati berbagai keunggulan SATUNADI untuk peningkatan layanan dan manajemen rumah sakit Anda.');
      cy.wait(2000);
      //service-1
      // cy.get('[alt="icon-menkes"]').should('exist')
      cy.get('[data-cy="services-1-title"]').should('contain', 'Terintegrasi SatuSehat')
      cy.get('[data-cy="services-1-desc"]').should('contain', 'Rekam medis elektronik SATUNADI  telah terintegrasi dengan SatuSehat sesuai Permenkes 24 tahun 2022 dan standar FHIR HL-7.');
      cy.get('[data-cy="collaborate-title"]').scrollIntoView();
      cy.wait(2000);
      //service-2
      // cy.get('[alt="icon-promotion"]').should('exist');
      cy.get('[data-cy="services-2-title"]').should('contain', 'Terintegrasi Supra System');
      cy.get('[data-cy="services-2-desc"]').should('contain', "SATUNADI telah terintegrasi dengan Supra System berupa asuransi BPJS, INACBG's, SISRUTE, SIRANAP dan SIRSONLINE dari Kemenkes.");
      cy.get('[data-cy="collaborate-title"]').scrollIntoView();
      cy.wait(2000);
      //service-3
      // cy.get('[alt="icon-computer"]').should('exist');
      cy.get('[data-cy="services-3-title"]').should('contain', 'Multiplatform Web dan Mobile');
      cy.get('[data-cy="services-3-desc"]').should('contain', 'SATUNADI dapat diakses melalui web dan mobile sehingga memudahkan tenaga kesehatan dan operasional rumah sakit.');
      cy.get('[data-cy="collaborate-title"]').scrollIntoView();
      cy.wait(2000);
      //service-4
      // cy.get('[alt="icon-data"]').should('exist');
      cy.get('[data-cy="services-4-title"]').should('contain', 'Perlindungan Data Terbaik');
      cy.get('[data-cy="services-4-desc"]').should('contain', 'Seluruh data rumah sakit dan pasien terjamin keamanannya dengan perlindungan terbaik.');
      // cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-5
      // cy.get('[alt="icon-monitor"]').should('exist');
      cy.get('[data-cy="services-5-title"]').should('contain', 'Real-time Monitoring');
      cy.get('[data-cy="services-5-desc"]').should('contain', 'Monitoring data secara real-time memudahkan manajemen dalam memantau dan menganalisis operasional rumah sakit.');
      // cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-6
      // cy.get('[alt="icon-maintain"]').should('exist');
      cy.get('[data-cy="services-6-title"]').should('contain','Bebas Pemeliharaan');
      cy.get('[data-cy="services-6-desc"]').should('contain','SATUNADI tidak membutuhkan pemeliharaan rutin sehingga tidak mengganggu operasional rumah sakit.');
      // cy.get('[alt="icon-promotion"]').scrollIntoView();
      cy.wait(2000);
      //service-7
      // cy.get('[alt="icon-deal"]').should('exist');
      cy.get('[data-cy="services-7-title"]').should('contain', 'Integrasi Layanan Rumah Sakit');
      cy.get('[data-cy="services-7-desc"]').should('contain', 'Dengan terintegrasinya layanan dan operasional rumah sakit melalui SATUNADI, maka kepuasan pasien akan semakin baik.');
      // cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
      //service-8
      // cy.get('[alt="icon-plan"]').should('exist');
      cy.get('[data-cy="services-8-title"]').should('contain', 'Peningkatan Efisiensi Kerja');
      cy.get('[data-cy="services-8-desc"]').should('contain', 'Monitoring data real-time akan mempermudah koordinasi sehingga operasional dan layanan rumah sakit lebih efisien.');
      // cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
      //service-9
      // cy.get('[alt="icon-cloud"]').should('exist');
      cy.get('[data-cy="services-9-title"]').should('contain', 'Kemudahan Evaluasi Bisnis');
      cy.get('[data-cy="services-9-desc"]').should('contain', 'Manajemen rumah sakit akan lebih mudah dalam pemantauan operasional serta pengambilan keputusan bisnis.');
      // cy.get('[alt="icon-monitor"]').scrollIntoView();
      cy.wait(2000);
    });
    it('Footer', () => {
      // Social & address
      cy.get('[data-cy="social-1"]').scrollIntoView();
      cy.get('[data-cy="footer-sponsor"]').should('exist');
      cy.get('[data-cy="leap-logo"]').should('exist');
      cy.get('[data-cy="footer-satunadi-logo"]').should('exist');
      cy.get('[data-cy="footer-address"]').should('contain','Menara Multimedia Lantai 14');
      cy.get('[data-cy="footer-socials"]').should('exist');
      //Linkedin
      cy.get('[data-cy="social-1"]').should('have.attr', 'href', 'https://www.linkedin.com/company/satunadi-id');
      // cy.get('[alt="Icon Linkedin"]').should('exist')
      cy.get('[data-cy="social-1"]').click();
      cy.wait(2000);
      //Instagram
      cy.visit(Url)
      cy.get('[data-cy="social-2"]').should('have.attr', 'href', 'https://www.instagram.com/satunadi.id');
      // cy.get('[alt="Icon Instagram"]').should('exist');
      cy.get('[data-cy="social-2"]').click();
      //Facebook
      cy.get('[data-cy="social-3"]').should('have.attr', 'href', 'https://www.facebook.com/people/Satunadi/61552280801346/');
      // cy.get('[alt="Icon Facebook"]').should('exist');
      cy.get('[data-cy="social-3"]').click();
      //Youtube
      cy.get('[data-cy="social-4"]').should('have.attr', 'href', 'https://www.youtube.com/channel/UCgiu6xUp1RNfjkwtTYzMhUA');
      // cy.get('[alt="Icon Youtube"]').should('exist');
      cy.get('[data-cy="social-4"]').click();
      //Tiktok
      cy.get('[data-cy="social-5"]').should('have.attr', 'href', 'https://www.tiktok.com/@satunadi.id');
      // cy.get('[alt="Icon Ti ktok"]').should('exist');
      cy.get('[data-cy="social-5"]').click();
      cy.wait(2000);
      // product & services - more link - copyright
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-producs-&-services"]').should('contain', 'Produk & Layanan');
      cy.get('[data-cy="footer-product-service-1"]').should('contain','Paket Basic');
      cy.get('[data-cy="footer-product-service-2"]').should('contain','Paket Advanced');
      cy.get('[data-cy="footer-product-service-3"]').should('contain','Modul Add On');
      cy.wait(2000);
      //Jelajahi lebih lanjut
      cy.get('[data-cy="footer-more-links"]').should('contain', 'Jelajah Lebih Lanjut');
      //tentang kami
      cy.get('[data-cy="menu-1"]').should('contain','Tentang Kami');
      cy.get('[data-cy="menu-1"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="menu-1"]').click()
      cy.wait(2000);
      //product
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-2"]').should('contain','Produk');
      cy.get('[data-cy="menu-2"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-2"]').click()
      cy.wait(2000);
      //artikel & berita
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-3"]').should('contain','Artikel & Berita');
      cy.get('[data-cy="menu-3"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-3"]').click()
      cy.wait(2000);
      //hubungi kami
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-4"]').should('contain','Hubungi Kami');
      cy.get('[data-cy="menu-4"]').scrollIntoView()
      cy.get(2000);
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      // cy.get('[alt="Logo Satunadi"]').click();
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-copyright"]').should('contain', '©2023 SATUNADI. All rights reserved');
      cy.wait(2000);
    });
  });
  describe('Product', () => {
    it('Navbar', () => {
      cy.get('[data-cy="menu-2"]').click();
      cy.get('[data-cy="navbar"]').scrollIntoView();
      cy.get('[data-cy="menu-1"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.wait(1000);
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
      cy.wait(1000);
    });
    it('Going to product', () => {
      cy.get('[data-cy="menu-2"]').click();
      //head
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
      cy.get('[data-cy="menu-1"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.wait(2000);
    });
    it('Product page', () => {
      cy.get('[data-cy="menu-2"]').click();
      cy.get('[data-cy="product-page-title"]').scrollIntoView();
      cy.get('[data-cy="product-page-title"]').should('contain', 'RINCIAN MODUL SATUNADI');
      cy.get('[data-cy="product-page-desc"]').should('contain','Berikut adalah rincian modul SATUNADI untuk berbagai operasional rumah sakit Anda');
      //modul-1
      cy.get('[data-cy="modul-1-title"]').should('contain','Paket Basic');
      cy.get('[data-cy="modul-1-title"]').click();
      cy.get('[data-cy="modul-1-desc-0"]').should('contain','Paket basic adalah paket digitalisasi sistem manajemen rumah sakit yang esensial untuk meningkatkan layanan kesehatan serta efisiensi operasional rumah sakit.');
      cy.get('[data-cy="modul-1-desc-1"]').should('contain','Paket basic terdiri dari modul registrasi, rekam medis elektronik, layanan gawat darurat, rawat jalan dan rawat inap, serta layanan pendukung lainnya.')
      cy.get('[data-cy="modul-1-list"]').scrollIntoView();
      cy.get('[data-cy="modul-1-list"]').should('exist');
      cy.get('[class="text-primary"]').should('contain','24');
      cy.contains('Modul Tersedia').should('exist');
      cy.contains('Registrasi (Loket Pendaftaran)').should('exist');
      cy.contains('Rekam Medis & Pelaporan').should('exist');
      cy.contains('Integrasi Platform SATUSEHAT').should('exist');
      cy.contains('Integrasi Asuransi BPJS').should('exist');
      cy.contains('Integrasi INACBGs').should('exist');
      cy.contains('Integrasi KEMENKES - SISRUTE').should('exist');
      cy.contains('Integrasi KEMENKES - SIRANAP').should('exist');
      cy.contains('Integrasi KEMENKES - SIRSONLINE').should('exist');
      cy.contains('Gawat Darurat').should('exist');
      cy.contains('Rawat Jalan').should('exist');
      cy.contains('Rawat Inap').should('exist');
      cy.contains('Gizi (Diet Nutrisi)').should('exist');
      cy.contains('Bedah Sentral').should('exist');
      cy.contains('Perawatan Intensif').should('exist');
      cy.contains('Radiologi').should('exist');
      cy.contains('Laboratorium Klinik').should('exist');
      cy.contains('Farmasi').should('exist');
      cy.contains('Billing System').should('exist');
      cy.contains('Logistik & Persediaan').should('exist');
      cy.contains('Registrasi Online').should('exist');
      cy.contains('Sistem Antrian Anjungan Mandiri').should('exist');
      cy.contains('Electronical Medical Record').should('exist');
      cy.contains('Administrasi Sistem (TI)').should('exist');
      cy.contains('Executive Information System').should('exist');
      cy.wait(2000);
      //modul-2
      cy.get('[data-cy="modul-2-title"]').should('contain','Paket Advanced');
      cy.get('[data-cy="modul-2-title"]').click();
      cy.get('[data-cy="modul-2-desc-0"]').should('contain','Paket advanced adalah paket digitalisasi sistem manajemen rumah sakit paling lengkap yang bisa Anda dapatkan.');
      cy.get('[data-cy="modul-2-desc-1"]').should('contain','Paket advanced terdiri dari seluruh modul paket basic dengan tambahan modul manajemen persalinan, ambulance, bank darah, kepegawaian serta modul operasional rumah sakit digital lainnya yang didesain khusus untuk kebutuhan Anda.');
      cy.get('[data-cy="modul-2-list"]').scrollIntoView();
      cy.get('[class="text-primary"]').should('contain','46');
      cy.contains('Modul Tersedia').should('exist');
      cy.get('[data-cy="package-2-second-title"]').should('contain','Sudah Termasuk 24 Modul Paket Basic');
      cy.get('[data-cy="modul-2-list"]').should('exist');
      cy.contains('Modul Integrasi Asuransi non BPJS').should('exist');
      cy.contains('Rehabilitasi Medis').should('exist');
      cy.contains('Kebidanan / VK Bersalin').should('exist');
      cy.contains('Pemeriksaan Kesehatan MCU').should('exist');
      cy.contains('Ambulance').should('exist');
      cy.contains('Pemulasaraan Jenazah').should('exist');
      cy.contains('Bank Darah').should('exist');
      cy.contains('Bendahara Penerimaan').should('exist');
      cy.contains('Bendahara Pengeluaran').should('exist');
      cy.contains('Kepegawaian (Administrasi)').should('exist');
      cy.contains('Hemodialisa').should('exist');
      cy.contains('Perawatan Sehari').should('exist');
      cy.contains('Anastesi').should('exist');
      cy.contains('Unit Layanan Pengadaan').should('exist');
      cy.contains('Pemeliharaan Sarana Rumah Sakit').should('exist');
      cy.contains('Laundry').should('exist');
      cy.contains('CSSD').should('exist');
      cy.contains('Bed Management').should('exist');
      cy.contains('Pengendalian dan Pencegahan Infeksi (PPI)').should('exist');
      cy.contains('Peningkatan Mutu dan Keselamatan').should('exist');
      cy.contains('Nurse Assistant').should('exist');
      cy.contains('Modul Covid - 19').should('exist');
      cy.wait(2000);
      //modul-3
      cy.get('[data-cy="modul-3-title"]').scrollIntoView();
      cy.get('[data-cy="modul-3-title"]').should('contain','Modul Add On');
      cy.get('[data-cy="modul-3-title"]').click();
      cy.get('[data-cy="modul-3-desc-0"]').should('contain','Modul Add On adalah modul layanan tambahan yang dapat Anda pilih bersama paket basic atau advanced sesuai kebutuhan Anda.');
      cy.get('[data-cy="modul-3-list"]').scrollIntoView();
      cy.get('[class="text-primary"]').should('contain','20');
      cy.contains('Modul Tersedia').should('exist');
      cy.get('[data-cy="modul-3-list"]').should('exist');
      cy.contains('Laboratory Information System (LIS)').should('exist');
      cy.contains('RIS - PACS').should('exist');
      cy.contains('Telemedicine').should('exist');
      cy.contains('Tele-ECG').should('exist');
      cy.contains('Remunerasi').should('exist');
      cy.contains('Akuntansi').should('exist');
      cy.contains('Kepegawaian (Payroll)').should('exist');
      cy.contains('Pendidikan, Pelatihan dan Pengembangan').should('exist');
      cy.contains('E-Office').should('exist');
      cy.contains('Pelayanan Kesehatan Kejiwaan').should('exist');
      cy.contains('Napza').should('exist');
      cy.contains('Sistem Terintegrasi dengan Layanan').should('exist');
      cy.contains('Humas').should('exist');
      cy.contains('Laboratorium Patologi Anatomi (PA)').should('exist');
      cy.contains('Management Inventarisasi Aset').should('exist');
      cy.contains('Sanitasi').should('exist');
      cy.contains('Modul Integrasi Akreditasi Sistem').should('exist');
      cy.contains('Modul Integrasi Kemenku - BIOS').should('exist');
      cy.contains('Modul Integrasi Kependudukan (DISDUK)').should('exist');
      cy.contains('Perencanaan (Planning)').should('exist')
      cy.wait(2000);
    });
    it('Footer', () => {
      // Social & address
      cy.get('[data-cy="social-1"]').scrollIntoView();
      cy.get('[data-cy="footer-sponsor"]').should('exist');
      cy.get('[data-cy="leap-logo"]').should('exist');
      cy.get('[data-cy="footer-satunadi-logo"]').should('exist');
      cy.get('[data-cy="footer-address"]').should('contain','Menara Multimedia Lantai 14');
      cy.get('[data-cy="footer-socials"]').should('exist');
      //Linkedin
      cy.get('[data-cy="social-1"]').should('have.attr', 'href', 'https://www.linkedin.com/company/satunadi-id');
      // cy.get('[alt="Icon Linkedin"]').should('exist')
      cy.get('[data-cy="social-1"]').click();
      cy.wait(2000);
      //Instagram
      cy.visit(Url)
      cy.get('[data-cy="social-2"]').should('have.attr', 'href', 'https://www.instagram.com/satunadi.id');
      // cy.get('[alt="Icon Instagram"]').should('exist');
      cy.get('[data-cy="social-2"]').click();
      //Facebook
      cy.get('[data-cy="social-3"]').should('have.attr', 'href', 'https://www.facebook.com/people/Satunadi/61552280801346/');
      // cy.get('[alt="Icon Facebook"]').should('exist');
      cy.get('[data-cy="social-3"]').click();
      //Youtube
      cy.get('[data-cy="social-4"]').should('have.attr', 'href', 'https://www.youtube.com/channel/UCgiu6xUp1RNfjkwtTYzMhUA');
      // cy.get('[alt="Icon Youtube"]').should('exist');
      cy.get('[data-cy="social-4"]').click();
      //Tiktok
      cy.get('[data-cy="social-5"]').should('have.attr', 'href', 'https://www.tiktok.com/@satunadi.id');
      // cy.get('[alt="Icon Ti ktok"]').should('exist');
      cy.get('[data-cy="social-5"]').click();
      cy.wait(2000);
      // product & services - more link - copyright
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-producs-&-services"]').should('contain', 'Produk & Layanan');
      cy.get('[data-cy="footer-product-service-1"]').should('contain','Paket Basic');
      cy.get('[data-cy="footer-product-service-2"]').should('contain','Paket Advanced');
      cy.get('[data-cy="footer-product-service-3"]').should('contain','Modul Add On');
      cy.wait(2000);
      //Jelajahi lebih lanjut
      cy.get('[data-cy="footer-more-links"]').should('contain', 'Jelajah Lebih Lanjut');
      //tentang kami
      cy.get('[data-cy="menu-1"]').should('contain','Tentang Kami');
      cy.get('[data-cy="menu-1"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="menu-1"]').click()
      cy.wait(2000);
      //product
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-2"]').should('contain','Produk');
      cy.get('[data-cy="menu-2"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-2"]').click()
      cy.wait(2000);
      //artikel & berita
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-3"]').should('contain','Artikel & Berita');
      cy.get('[data-cy="menu-3"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-3"]').click()
      cy.wait(2000);
      //hubungi kami
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-4"]').should('contain','Hubungi Kami');
      cy.get('[data-cy="menu-4"]').scrollIntoView()
      cy.get(2000);
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      // cy.get('[alt="Logo Satunadi"]').click();
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-copyright"]').should('contain', '©2023 SATUNADI. All rights reserved');
      cy.wait(2000);
    });
  });
  describe('Article & News', () => {
    it('Navbar', () => {
      cy.get('[data-cy="menu-3"]').click();
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-1"]').should('exist');
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
      cy.wait(2000);
    });
    it('Article & news', () => {
      cy.get('[data-cy="menu-3"]').click();
      cy.get('[data-cy="article-title"]').should('contain','ARTIKEL & BERITA');
      cy.get('[data-cy="article-desc"]').should('contain','Pilihan berita terkait SATUNADI dan kesehatan untuk Anda');
      //content
      cy.get('[data-cy="content-title-1"]').should('exist');
      cy.get('[data-cy="content-title-2"]').should('exist');
      //article-1
      cy.get('[data-cy="content-date-1"]').should('exist');
      //article-2
      cy.get('[data-cy="content-date-2"]').should('exist');
      // cy.get('[data-cy="content-date-2"]').scrollIntoView();
      //article-3
      cy.get('[data-cy="content-date-3"]').should('exist');
      cy.wait(2000);
      //article-4
      cy.get('[data-cy="content-date-4"]').should('exist');
      //article-5
      // cy.get('[data-cy="content-date-5"]').should('exist');
      cy.get('[data-cy="content-date-5"]').scrollIntoView();
      //article-6
      cy.get('[data-cy="content-date-6"]').should('exist');
      cy.wait(2000);
    });
    it('Footer', () => {
      // Social & address
      cy.get('[data-cy="social-1"]').scrollIntoView();
      cy.get('[data-cy="footer-sponsor"]').should('exist');
      cy.get('[data-cy="leap-logo"]').should('exist');
      cy.get('[data-cy="footer-satunadi-logo"]').should('exist');
      cy.get('[data-cy="footer-address"]').should('contain','Menara Multimedia Lantai 14');
      cy.get('[data-cy="footer-socials"]').should('exist');
      //Linkedin
      cy.get('[data-cy="social-1"]').should('have.attr', 'href', 'https://www.linkedin.com/company/satunadi-id');
      // cy.get('[alt="Icon Linkedin"]').should('exist')
      cy.get('[data-cy="social-1"]').click();
      cy.wait(2000);
      //Instagram
      cy.visit(Url)
      cy.get('[data-cy="social-2"]').should('have.attr', 'href', 'https://www.instagram.com/satunadi.id');
      // cy.get('[alt="Icon Instagram"]').should('exist');
      cy.get('[data-cy="social-2"]').click();
      //Facebook
      cy.get('[data-cy="social-3"]').should('have.attr', 'href', 'https://www.facebook.com/people/Satunadi/61552280801346/');
      // cy.get('[alt="Icon Facebook"]').should('exist');
      cy.get('[data-cy="social-3"]').click();
      //Youtube
      cy.get('[data-cy="social-4"]').should('have.attr', 'href', 'https://www.youtube.com/channel/UCgiu6xUp1RNfjkwtTYzMhUA');
      // cy.get('[alt="Icon Youtube"]').should('exist');
      cy.get('[data-cy="social-4"]').click();
      //Tiktok
      cy.get('[data-cy="social-5"]').should('have.attr', 'href', 'https://www.tiktok.com/@satunadi.id');
      // cy.get('[alt="Icon Ti ktok"]').should('exist');
      cy.get('[data-cy="social-5"]').click();
      cy.wait(2000);
      // product & services - more link - copyright
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-producs-&-services"]').should('contain', 'Produk & Layanan');
      cy.get('[data-cy="footer-product-service-1"]').should('contain','Paket Basic');
      cy.get('[data-cy="footer-product-service-2"]').should('contain','Paket Advanced');
      cy.get('[data-cy="footer-product-service-3"]').should('contain','Modul Add On');
      cy.wait(2000);
      //Jelajahi lebih lanjut
      cy.get('[data-cy="footer-more-links"]').should('contain', 'Jelajah Lebih Lanjut');
      //tentang kami
      cy.get('[data-cy="menu-1"]').should('contain','Tentang Kami');
      cy.get('[data-cy="menu-1"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="menu-1"]').click()
      cy.wait(2000);
      //product
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-2"]').should('contain','Produk');
      cy.get('[data-cy="menu-2"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-2"]').click()
      cy.wait(2000);
      //artikel & berita
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-3"]').should('contain','Artikel & Berita');
      cy.get('[data-cy="menu-3"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-3"]').click()
      cy.wait(2000);
      //hubungi kami
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-4"]').should('contain','Hubungi Kami');
      cy.get('[data-cy="menu-4"]').scrollIntoView()
      cy.get(2000);
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      // cy.get('[alt="Logo Satunadi"]').click();
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-copyright"]').should('contain', '©2023 SATUNADI. All rights reserved');
      cy.wait(2000);
    });
  });
  describe('Help center', () => {
    it('Navbar', () => {
      cy.get('[data-cy="menu-4"]').click();
      cy.get('[data-cy="navbar"]').scrollIntoView();
      cy.get('[data-cy="menu-1"]').should('exist');
      cy.get('[data-cy="menu-2"]').should('exist');
      cy.get('[data-cy="menu-3"]').should('exist');
      cy.get('[data-cy="menu-4"]').should('exist');
      cy.get(1500);
      // cy.get('[alt="Logo Satunadi"]').should('have.attr', 'src', '/images/logo-satunadi.svg');
      cy.get('[data-cy="navbar-list"]').should('exist');
      cy.get('[data-cy="navbar-try-btn"]').should('contain', 'Coba Sekarang');
      cy.get(1500);
    });
    it('Demo Form', () => {
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      cy.get('[data-cy="help-center-desc"]').scrollIntoView();
      // form 1
      // cy.get('button').contains('Mulai Sekarang').click();
      cy.get('[data-cy="demo-form-cta-title"]').should('contain','Formulir Pengajuan Demo SATUNADI');
      cy.get('[data-cy="demo-form-cta-desc"]').should('contain','Pelajari lebih lanjut mengenai SATUNADI dengan mengisi formulir pengajuan demo berikut.');
      cy.get('[data-cy="demo-form-cta-btn"]').click();
      cy.get('[data-cy="demo-form-name-input"]').type("contoh");
      cy.get('[data-cy="demo-form-phone-input"]').type('8567876523');
      cy.get('[data-cy="demo-form-email-input"]').type('cek@mail.com');
      cy.get('[data-cy="demo-form-back-btn"]').should('be.enabled');
      cy.get('[data-cy="demo-form-back-btn"]').should('contain','Kembali Ke Beranda');
      cy.get('[data-cy="demo-form-submit-btn"]').click();
      //form 2
      cy.get('[data-cy="help-center-desc"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="demo-form-position-input"]').type('IT Consultan');
      cy.get('[data-cy="demo-form-fasyankes-input"]').type('Klinik Mandiri');
      // cy.get('[data-cy="demo-form-province-input"]').click();
      // cy.get('[data-cy="demo-form-province-input"]').type('bali');
      // cy.get('[data-cy="demo-form-regency-input"]').click();
      // cy.get('[data-cy="demo-form-regency-input"]').type('Kabupaten Simeulue');
      // cy.get('[data-cy="demo-form-subdistrict-input"]').click();
      // cy.get('[data-cy="demo-form-subdistrict-input"]').type('Teupah Selatan');
      // cy.get('[data-cy="demo-form-village-input"]').click();
      // cy.get('[data-cy="demo-form-village-input"]').type('Labuhan Bajau');
      // cy.get('[data-cy="demo-form-postalcode-input"]').type('1234');
      // cy.get('[data-cy="demo-form-address-input"]').type('Jalan Apa saja Yang penting sama kamu');
      // cy.get('[data-cy="demo-form-back-btn"]').should('be.enabled');
      // cy.get('[data-cy="demo-form-back-btn"]').should('contain','Sebelumnya');
      // cy.get('[data-cy="demo-form-submit-btn"]').click();
      //form 3
      // cy.get('[data-cy="help-center-desc"]').scrollIntoView();
      // cy.wait(2000);
      // cy.get('[data-cy="demo-form-needs-input"]').type('Keperluan Digitalisasi SIMRS dan Eklinik');
      // cy.get('[data-cy="demo-form-accreditation-input-1"]').click();
      // cy.get('[data-cy="demo-form-sit-input-1"]').click();
      // cy.get('[data-cy="demo-form-tipe-input-1"]').click();
      // cy.get('[data-cy="demo-form-back-btn"]').should('be.enabled');
      // cy.get('[data-cy="demo-form-back-btn"]').should('contain','Sebelumnya');
      // cy.get('[data-cy="demo-form-submit-btn"]').click();
      // cy.wait(10000)
      //validasi form
      // cy.get('[data-cy="help-center-desc"]').scrollIntoView();
      // cy.get('[data-cy="demo-form-submitted-title"]').contains('Terimakasih Telah Mengisi Form');
      // cy.get('[data-cy="demo-form-submitted-desc"]').contains('Tim kami dengan senang hati akan segera menghubungi Anda untuk memberikan informasi terbaru. Kami sangat menghargai ketertarikan dan kepercayaan Anda kepada kami. Dengan penuh dedikasi.');
      // cy.get('[data-cy="demo-form-submitted-btn"]').should('be.enabled');
    })
    it('Help center', () => {
      //title
      cy.get('[data-cy="menu-4"]').click();
      cy.get('[data-cy="navbar"]').scrollIntoView();
      cy.get('[data-cy="help-center-title"]').should('contain','HUBUNGI KAMI');
      cy.get('[data-cy="help-center-desc"]').should('contain','Segera isi form dibawah ini untuk mengajukan demo aplikasi dengan Tim pelayanan kami.');
      cy.get('[data-cy="help-center-title"]').scrollIntoView();
      cy.wait(1500);
      
    });
    it('FAQ', () => {
      //FAQ
      cy.get('[data-cy="menu-4"]').click();
      //Q-1
      cy.get('[data-cy="faq-title"]').scrollIntoView();
      cy.get('[data-cy="faq-title"]').should('contain','Pertanyaan yang sering ditanyakan');
      cy.get('[data-cy="modul-1-title"]').should('contain','Bagaimana cara mengajukan demo produk?');
      cy.get('[data-cy="modul-1-title"]').click();
      cy.get('[data-cy="faq-title"]').scrollIntoView();
      cy.get('[data-cy="modul-1-desc-0"]').should('contain','Silakan klik “Coba Sekarang” isi data yang dibutuhkan untuk mengajukan demo produk. Selanjutnya tim SATUNADI akan menghubungi Anda untuk menjadwalkan demo produk.');
      cy.wait(2000);
      //Q-2
      cy.get('[data-cy="modul-2-title"]').should('contain','Berapa lama waktu yang dibutuhkan untuk mengikuti demo produk SATUNADI?');
      cy.get('[data-cy="modul-2-title"]').click();
      cy.get('[data-cy="modul-2-desc-0"]').should('contain','Demo produk biasanya membutuhkan waktu selama kurang lebih satu jam dan dilaksanakan secara online menggunakan Zoom atau aplikasi teleconference lainnya.');
      cy.wait(2000);
      //Q-3
      cy.get('[data-cy="modul-3-title"]').should('contain','Berapa biaya yang dikeluarkan untuk mengimplementasikan SATUNADI?');
      cy.get('[data-cy="modul-3-title"]').click();
      cy.get('[data-cy="modul-3-desc-0"]').should('contain','SATUNADI menawarkan beberapa model kerjasama, termasuk di antaranya terdapat biaya tambahan untuk modul add-on maupun layanan kustomisasi SIMRS. Untuk lebih jelas, silakan hubungi tim SATUNADI.');
      cy.wait(2000);
      //Q-4
      cy.get('[data-cy="modul-4-title"]').should('contain','Apakah pada saat implementasi awal disediakan pendampingan untuk membantu operasional?');
      cy.get('[data-cy="modul-4-title"]').click();
      cy.get('[data-cy="modul-4-desc-0"]').should('contain','SATUNADI berkomitmen untuk memberikan layanan terbaik dengan menyediakan tim teknis dalam mendampingi set-up dan training awal bagi staf rumah sakit.');
      // cy.contains('1) Tingkat kesulitan dari kustomisasi yang dibutuhkan').should('exist');
      // cy.contains('2) Kesepakatan waktu & cakupan pekerjaan antara rumah sakit dan tim teknis SATUNADI').should('exist');
      cy.wait(2000);
      //Q-5
      cy.get('[data-cy="faq-title"]').scrollIntoView();
      cy.get('[data-cy="modul-5-title"]').should('contain','Apakah memungkinkan untuk melakukan kustomisasi fitur atau layanan?');
      cy.get('[data-cy="modul-5-title"]').click();
      cy.get('[data-cy="modul-5-desc-0"]').should('contain','Ya. Anda bisa mengajukan kustomisasi fitur dan layanan SATUNADI sesuai dengan kebutuhan Anda.');
      cy.wait(2000);
      //Q-6
      cy.get('[data-cy="modul-6-title"]').should('contain','Berapa lama yang dibutuhkan untuk pengerjaan kustomisasi?');
      cy.get('[data-cy="modul-6-title"]').click();
      cy.get('[data-cy="modul-6-desc-0"]').should('contain','Lama pengerjaan kustomisasi tergantung pada kerumitan kustomisasi yang dibutuhkan. Selain itu waktu kustomisasi fitur SATUNADI juga bergantung pada kesepakatan waktu pengerjaan pekerjaan antara rumah sakit dan tim teknis SATUNADI.');
      cy.wait(2000);
      //Q-7
      cy.get('[data-cy="modul-7-title"]').should('contain','Berapa biaya yang harus saya keluarkan untuk melakukan kustomisasi?');
      cy.get('[data-cy="modul-7-title"]').click();
      cy.get('[data-cy="modul-7-desc-0"]').should('contain','Biaya kustomisasi fitur tergantung kebutuhan. Untuk biayanya tim SATUNADI akan membuat surat penawaran harga sesuai permintaan kustomisasi yang diajukan rumah sakit.');
      cy.wait(2000);
    });
    it('Footer', () => {
      // Social & address
      cy.get('[data-cy="social-1"]').scrollIntoView();
      cy.get('[data-cy="footer-sponsor"]').should('exist');
      cy.get('[data-cy="leap-logo"]').should('exist');
      cy.get('[data-cy="footer-satunadi-logo"]').should('exist');
      cy.get('[data-cy="footer-address"]').should('contain','Menara Multimedia Lantai 14');
      cy.get('[data-cy="footer-socials"]').should('exist');
      //Linkedin
      cy.get('[data-cy="social-1"]').should('have.attr', 'href', 'https://www.linkedin.com/company/satunadi-id');
      // cy.get('[alt="Icon Linkedin"]').should('exist')
      cy.get('[data-cy="social-1"]').click();
      cy.wait(2000);
      //Instagram
      cy.visit(Url)
      cy.get('[data-cy="social-2"]').should('have.attr', 'href', 'https://www.instagram.com/satunadi.id');
      // cy.get('[alt="Icon Instagram"]').should('exist');
      cy.get('[data-cy="social-2"]').click();
      //Facebook
      cy.get('[data-cy="social-3"]').should('have.attr', 'href', 'https://www.facebook.com/people/Satunadi/61552280801346/');
      // cy.get('[alt="Icon Facebook"]').should('exist');
      cy.get('[data-cy="social-3"]').click();
      //Youtube
      cy.get('[data-cy="social-4"]').should('have.attr', 'href', 'https://www.youtube.com/channel/UCgiu6xUp1RNfjkwtTYzMhUA');
      // cy.get('[alt="Icon Youtube"]').should('exist');
      cy.get('[data-cy="social-4"]').click();
      //Tiktok
      cy.get('[data-cy="social-5"]').should('have.attr', 'href', 'https://www.tiktok.com/@satunadi.id');
      // cy.get('[alt="Icon Ti ktok"]').should('exist');
      cy.get('[data-cy="social-5"]').click();
      cy.wait(2000);
      // product & services - more link - copyright
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-producs-&-services"]').should('contain', 'Produk & Layanan');
      cy.get('[data-cy="footer-product-service-1"]').should('contain','Paket Basic');
      cy.get('[data-cy="footer-product-service-2"]').should('contain','Paket Advanced');
      cy.get('[data-cy="footer-product-service-3"]').should('contain','Modul Add On');
      cy.wait(2000);
      //Jelajahi lebih lanjut
      cy.get('[data-cy="footer-more-links"]').should('contain', 'Jelajah Lebih Lanjut');
      //tentang kami
      cy.get('[data-cy="menu-1"]').should('contain','Tentang Kami');
      cy.get('[data-cy="menu-1"]').scrollIntoView();
      cy.wait(2000);
      cy.get('[data-cy="menu-1"]').click()
      cy.wait(2000);
      //product
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-2"]').should('contain','Produk');
      cy.get('[data-cy="menu-2"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-2"]').click()
      cy.wait(2000);
      //artikel & berita
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-3"]').should('contain','Artikel & Berita');
      cy.get('[data-cy="menu-3"]').scrollIntoView()
      cy.wait(2000);
      cy.get('[data-cy="menu-3"]').click()
      cy.wait(2000);
      //hubungi kami
      cy.get('[data-cy="logo-satunadi"]').click();
      cy.get('[data-cy="menu-4"]').should('contain','Hubungi Kami');
      cy.get('[data-cy="menu-4"]').scrollIntoView()
      cy.get(2000);
      cy.get('[data-cy="menu-4"]').click();
      cy.wait(2000);
      // cy.get('[alt="Logo Satunadi"]').click();
      cy.get('[data-cy="footer-copyright"]').scrollIntoView();
      cy.get('[data-cy="footer-copyright"]').should('contain', '©2023 SATUNADI. All rights reserved');
      cy.wait(2000);
    });
  });
});