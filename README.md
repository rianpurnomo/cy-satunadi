# prepare with mac
- install node
- install npm (brew install npm)
- npm -v (check version npm)
- npm install

# use run
- npx cypress open (without record)
- npx cypress run (with record)

# run coverage 
- npx nyc cypress run
- npx nyc report --reporter=text-summary
